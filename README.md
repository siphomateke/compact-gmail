# Compact Gmail

## [Deprecated]: This project has been merged into [compact-website-styles](https://gitlab.com/siphomateke/compact-website-styles)

Browser extension that makes the new Gmail redesign more compact.
